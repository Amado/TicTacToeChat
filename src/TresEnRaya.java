

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


/**
 * Clase principal del juego, inicializa el juego y la interfaz gráfica
 * @author emilio
 *
 */
public class TresEnRaya extends JFrame
{
	
	/**
	 * Atributos
	 */
	
	//Sockets de servidor, client y streams
	public static ServerSocket serverSocket = null;
	public static Socket clientSocket = null;
	public static ObjectInputStream objectInputStream = null;
	public static ObjectOutputStream objectOutputStream = null;
	
	//ip y puerto
	public static final String ip = "127.0.0.1";
	public static final int port = 12345;

	//las dimensiones de la pantalla								
	private static final int screeWidth = 500;												
	private static final int screeHeight = 550;												
	
	//creamos las celdas para el tablero de juego, 9 celdas
	public static JButton cell00, cell01, cell02;	
	public static JButton cell10, cell11, cell12;
	public static JButton cell20, cell21, cell22;
	
	//Text Area para ver los mensajes del chat
	public static JTextArea taChat;										
	private JScrollPane chatScrollPane;											
	
	//nick deben ser indicado tanto para crear una sesion de juego
	//como para unirse a una sesión creada por un servidor
	public static JTextField tfNick; 
	
	//TextField para introducir mensajes para el chat
	public static JTextField tfChatMessage;
	
	//botones para la creación de sesión y unión a sesión
	public static JButton btJoinGame, btCreateGame; 						
	
	//tablero de juego que contendrá "X" u "O" para las celdas
	public static String gameBoard[] = { "","","", 
								         "","","", 
								         "","","" };

	//contendrá el caracter "X" u "O"
	//server siempre da "X"
	//client siempre da "O"
	public static String xo = ""; 
	
	
	public static String serverNick, clientNick;
	public static String chatMessage; 			
	
	//de quién es el turno
	public static boolean whoseTurn; 				
	
	//número de movimientos, se declara empate al llegar a 9
	public static int movesNumber = 0;					
	
	
	/**
	 * Constructor
	 */
	public TresEnRaya()
	{
		//inicializamos la gui
		initializeGui();
	}
	
	
	/**
	 * Inicializamos la interfaz
	 */
	private void initializeGui()
	{
		
		//establecemos los prámetros de la ventana
		setSize(screeWidth, screeHeight); 			
		setResizable(true);
		setTitle("3 en raya con chat");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		
		getContentPane().setLayout(new BorderLayout());
		
		
		//PANEL DE LAS CELDAS
		//===================
		
		//JPanel para ubicar el tablero de las celdas
		JPanel cellsPanel = new JPanel();
		cellsPanel.setLayout(new GridLayout(3, 3, 5, 5));
				
		cell00 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell00.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell00.setText(xo);
					GameOperations.sendMessage(xo + "1");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell00.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[0] = "X";
					}
					else
					{
						gameBoard[0] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell00);
		
		cell01 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell01.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell01.setText(xo);
					GameOperations.sendMessage(xo + "2");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell01.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[1] = "X";
					}
					else
					{
						gameBoard[1] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell01);
		
		cell02 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell02.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell02.setText(xo);
					GameOperations.sendMessage(xo + "3");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell02.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[2] = "X";
					}
					else
					{
						gameBoard[2] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell02);
		
		cell10 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell10.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell10.setText(xo);
					GameOperations.sendMessage(xo + "4");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell10.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[3] = "X";
					}
					else
					{
						gameBoard[3] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell10);
		
		cell11 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell11.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell11.setText(xo);
					GameOperations.sendMessage(xo + "5");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell11.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[4] = "X";
					}
					else
					{
						gameBoard[4] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell11);
		
		cell12 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell12.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell12.setText(xo);
					GameOperations.sendMessage(xo + "6");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell12.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[5] = "X";
					}
					else
					{
						gameBoard[5] = "O";
					}					
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell12);
		
		cell20 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell20.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell20.setText(xo);
					GameOperations.sendMessage(xo + "7");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell20.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[6] = "X";
					}
						
					else
					{
						gameBoard[6] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell20);
		
		cell21 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell21.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell21.setText(xo);
					GameOperations.sendMessage(xo + "8");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell21.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[7] = "X";
					}
					else
					{
						gameBoard[7] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}
			}
		});
		cellsPanel.add(cell21);
		
		cell22 = new JButton();
		/*listener de la celda que dispara las siguientes acciones:
		 * 		
		 * 		determina de quíen es el próximo turno
		 * 		deshabilita la celda
		 * 		marca la celda con una "X" u "O"
		 * 		suma 1 al toal de movimientos
		 * 		determina si hay vencedor*/
		cell22.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(whoseTurn)
				{
					cell22.setText(xo);
					GameOperations.sendMessage(xo + "9");
					GameOperations.sendMessage("true");
					whoseTurn = false;
					cell22.setEnabled(false);
					if(xo.equals("X"))
					{
						gameBoard[8] = "X";
					}
					else
					{
						gameBoard[8] = "O";
					}
					++movesNumber;
					GameOperations.checkCells();
				}	
			}
		});
		cellsPanel.add(cell22);
		
		//desactivamos los botones hasta que se realice la 
		//conexión server <-> client
		GameOperations.setCellsEnabled(false); 	
		
		//añadimos el panel de celdas a la ventana
		getContentPane().add(cellsPanel, BorderLayout.CENTER);
		
		
		//PANEL DEL MENSAJE
		//================
		
		//creamos el panel para el emnsaje
		JPanel messagePanel = new JPanel();
		messagePanel.setLayout(new BorderLayout());
		messagePanel.setPreferredSize(new Dimension(screeWidth, 150));
		tfChatMessage = new JTextField(" ");
		tfChatMessage.setEditable(false);
		/* listener del textField del mensaje que se envía
		 * dispara las siguientes acciones :
		 * 
		 * 		añade el texto escrito al textArea para el chat
		 * 		baja la línea de escritura al fondo del textArea
		 * 		manda el mensaje al otro usuario
		 * 		vacía el textField del mensaje
		 */
		tfChatMessage.addKeyListener(new KeyAdapter()
		{
			public void keyPressed(KeyEvent e) 
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					taChat.append(tfNick.getText() + ":" + tfChatMessage.getText() + "\n");
					GameOperations.scrollToBottom();
					GameOperations.sendMessage(tfChatMessage.getText());
					tfChatMessage.setText(" ");
				}
			}
		});
		
		FlowLayout fl_tfChatPanel = new FlowLayout();
		fl_tfChatPanel.setAlignment(FlowLayout.LEFT);
		JPanel tfChatPanel = new JPanel(fl_tfChatPanel);
		JLabel lbEscribe = new JLabel("Escribe : ");
		tfChatPanel.add(lbEscribe);
		tfChatMessage.setColumns(37);
		tfChatPanel.add(tfChatMessage);
		
		messagePanel.add(tfChatPanel, BorderLayout.SOUTH);
		
		//añadimos el panel a la ventana
		getContentPane().add(messagePanel, BorderLayout.SOUTH);
		
		
		//PANEL DEL CHAT
		//=============
		
		// creamos el panel para visualizar los mensajes de chat
		JPanel chatPanel = new JPanel();
		chatPanel.setLayout(new BorderLayout());
		chatPanel.setPreferredSize(new Dimension(screeWidth, 100));
		taChat = new JTextArea();
		taChat.setLineWrap(true);
		taChat.setEditable(false);
		chatScrollPane = new JScrollPane(taChat); 
		chatScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		chatPanel.add(chatScrollPane, BorderLayout.CENTER);
		
		//añadimos el panel 
		messagePanel.add(chatPanel, BorderLayout.CENTER);
		
		
		tfNick = new JTextField();
		tfNick.setToolTipText("Introduce tu nick");
		tfNick.setColumns(10);
		
		
		//CREACIÓN DE LA SESIÓN DE JUEGO
		//==============================
		
		//esta acción solo la realiza el jugador que desea ser el servidor
		//uan vez creada la sesión, el orto jugador solo podrá ser cliente
		
		btCreateGame = new JButton(" Crear ");
		btCreateGame.setToolTipText("Crea una nueva sesión de juego");
		/* listener del botón de creación de nueva sesión de juego 
		 * dispara las siguientes acciones :
		 * 
		 * 		avisa si el usuario no se a puesto un nick
		 * 		crea una nueva sesión si el usuario tiene nick
		 */
		btCreateGame.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event) 
			{
				if(tfNick.getText().equals(""))
				{
					GameOperations.sendMessage("NO TE HAS PUESTO UN NICK!");
					TresEnRaya.taChat.append("NO TE HAS PUESTO UN NICK!\n");
					GameOperations.scrollToBottom();
					return;
				}
				
				//creamos una nueva sesión de servidor, la demos iniciar en
				//un nuevo thread mientras esperamos la conexión del cliente
				new CreateServerConnection();
			}
		});
	
		
		// UNIRSE A UNA SESIÓN CREADA
		//===========================
		
		//esta acción solo la realiza el jugador que desea unirse
		//a una sesión de juego ya creada por el jugador que hace de servidor
		
		btJoinGame = new JButton("Unirse");
		btJoinGame.setToolTipText("Únete a una sesión de juego");
		/* listener del botón de unión a sesión de juego 
		 * dispara las siguientes acciones :
		 * 
		 * 		avisa si el usuario no se a puesto un nick
		 * 		se une a una sesión si el usuario tiene nick
		 */
		btJoinGame.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event) 
			{
	
					if(tfNick.getText().equals(""))
					{
						GameOperations.sendMessage("NO TE HAS PUESTO UN NICK!");
						TresEnRaya.taChat.append("NO TE HAS PUESTO UN NICK!\n");
						GameOperations.scrollToBottom();
						return;
					}
					
					//iniciamos una nueva conexión de cliente
					CreateClientConnection.connectClient();
			}
		});
		
		//PANEL SUPERIOR DE NICK, BOTON CREAR, BOTON UNIRSE
		//=================================================
		
		JPanel pNorth = new JPanel();
		
		JLabel lbNick = new JLabel("Nick :");
		pNorth.add(lbNick);
		
		pNorth.add(tfNick);
		
		JLabel separador1 = new JLabel("          ");
		pNorth.add(separador1);
		
		pNorth.add(btCreateGame);
		
		JLabel separador2 = new JLabel("          ");
		pNorth.add(separador2);
		
		pNorth.add(btJoinGame);
		getContentPane().add(pNorth, BorderLayout.NORTH);

	}

	
	/**
	 * Arranca el programa
	 * @param args
	 */
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				new TresEnRaya().setVisible(true);				
			}
		});
	}
}
