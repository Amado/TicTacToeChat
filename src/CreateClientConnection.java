import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Clase que conecta el socket del cliente al socket del servidor
 * y manda mensajes al sistema
 * @author emilio
 *
 */
public class CreateClientConnection {

	public static void connectClient() {
		try {
			//creamos un socket de cliente con la ip y puertos del servidor
			TresEnRaya.clientSocket = new Socket(TresEnRaya.ip, TresEnRaya.port);

			//iniciamos los streams del cliente
			TresEnRaya.objectOutputStream = new ObjectOutputStream(TresEnRaya.clientSocket.getOutputStream());
			TresEnRaya.objectOutputStream.flush();
			TresEnRaya.objectInputStream = new ObjectInputStream(TresEnRaya.clientSocket.getInputStream());

			TresEnRaya.chatMessage = (String) TresEnRaya.objectInputStream.readObject();
			
			//añadimos mensajes al chat del cliente
			TresEnRaya.taChat.append(TresEnRaya.chatMessage + "\n");
			GameOperations.scrollToBottom();

			//turno del cliente
			//escribimos una O
			TresEnRaya.xo = "O";
			TresEnRaya.whoseTurn = false;

			//informamos al sistema del nick usado
			TresEnRaya.clientNick = TresEnRaya.tfNick.getText();
			GameOperations.sendMessage(TresEnRaya.clientNick);

			//nick recibido del server
			TresEnRaya.chatMessage = (String) TresEnRaya.objectInputStream.readObject(); 
			TresEnRaya.serverNick = "" + TresEnRaya.chatMessage;
			
			//como todo el mudo tiene nick podemos iniciar el juego
			
			//habilitamos las celdas y el chat
			GameOperations.setCellsEnabled(true);
			TresEnRaya.tfChatMessage.setEditable(true);

			TresEnRaya.tfNick.setEnabled(false);

			TresEnRaya.taChat.append("El servidor juega primero!\n");
			GameOperations.scrollToBottom();

			//deshabilitamos el panel superior, crear, unirse y nick
			TresEnRaya.btJoinGame.setEnabled(false);
			TresEnRaya.btCreateGame.setEnabled(false);
			TresEnRaya.tfNick.setEnabled(false);

			//thread para que el sistema reciba datos del servidor
			new ReceiveMessage("Mensaje del servidor");
			
		} catch (Exception e) {
			e.printStackTrace();
			TresEnRaya.taChat.append("EL SERVIDOR NO ESTÁ ONLINE!\n");
			GameOperations.scrollToBottom();
		}
	}

}
