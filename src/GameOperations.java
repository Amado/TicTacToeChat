


import java.net.SocketException;


/**
 * Clase para levar a cabo las difrentes operaciones en torno a la gestión del juego
 * 
 * Operaciones :
 * 
 * 		Comprobar valores de celdas y establecer vencedor
 * 
 * 		Activar/desactivar celdas de juego
 * 
 * 		Enviar mensajes entre jugadores
 * 
 * 		Cerrar todas las conexiones al finalizar el juego
 * 
 * 		Mover la linea del chat hacia abajo
 * 
 * @author emilio
 *
 */
public class GameOperations {
	
	
	/**
	 * comprueba en cada turno el contenido de las celdas para determinar el ganador del juego
	 */
	public static void checkCells()
	{
		//checks if the server wins the game
		if
		(
			// celdas verticales
			(TresEnRaya.gameBoard[0].equals("X") && 
			 TresEnRaya.gameBoard[1].equals("X") && 
			 TresEnRaya.gameBoard[2].equals("X")) || 
			(TresEnRaya.gameBoard[3].equals("X") && 
			 TresEnRaya.gameBoard[4].equals("X") && 
			 TresEnRaya.gameBoard[5].equals("X")) ||
			(TresEnRaya.gameBoard[6].equals("X") && 
			 TresEnRaya.gameBoard[7].equals("X") && 
			 TresEnRaya.gameBoard[8].equals("X")) ||
			 
			// celdas horizontales
			(TresEnRaya.gameBoard[0].equals("X") && 
			 TresEnRaya.gameBoard[3].equals("X") && 
			 TresEnRaya.gameBoard[6].equals("X")) ||
			(TresEnRaya.gameBoard[1].equals("X") && 
			 TresEnRaya.gameBoard[4].equals("X") && 
			 TresEnRaya.gameBoard[7].equals("X")) ||
			(TresEnRaya.gameBoard[2].equals("X") && 
			 TresEnRaya.gameBoard[5].equals("X") && 
			 TresEnRaya.gameBoard[8].equals("X")) ||
			 
			// celdas diagoanles
			(TresEnRaya.gameBoard[0].equals("X") && 
			 TresEnRaya.gameBoard[4].equals("X") && 
			 TresEnRaya.gameBoard[8].equals("X")) ||
			(TresEnRaya.gameBoard[2].equals("X") &&
			 TresEnRaya.gameBoard[4].equals("X") && 
			 TresEnRaya.gameBoard[6].equals("X"))
		)
		{
			//si el servidor gana informamos por la ventana del chat
			setCellsEnabled(false);
			TresEnRaya.taChat.append(TresEnRaya.serverNick + " ha ganado!!!!" + "\n");
			GameOperations.scrollToBottom();
		}

		//checks if the client wins the game
		else if
		(
				// celdas verticales
				(TresEnRaya.gameBoard[0].equals("O") && 
				 TresEnRaya.gameBoard[1].equals("O") && 
				 TresEnRaya.gameBoard[2].equals("O")) ||
				(TresEnRaya.gameBoard[3].equals("O") && 
				 TresEnRaya.gameBoard[4].equals("O") && 
				 TresEnRaya.gameBoard[5].equals("O")) ||
				(TresEnRaya.gameBoard[6].equals("O") && 
				 TresEnRaya.gameBoard[7].equals("O") && 
				 TresEnRaya.gameBoard[8].equals("O")) ||
				// celdas horizontales
				(TresEnRaya.gameBoard[0].equals("O") && 
				 TresEnRaya.gameBoard[3].equals("O") && 
				 TresEnRaya.gameBoard[6].equals("O")) ||
				(TresEnRaya.gameBoard[1].equals("O") && 
				 TresEnRaya.gameBoard[4].equals("O") && 
				 TresEnRaya.gameBoard[7].equals("O")) ||
				(TresEnRaya.gameBoard[2].equals("O") && 
				 TresEnRaya.gameBoard[5].equals("O") && 
				 TresEnRaya.gameBoard[8].equals("O")) ||
				// celdas diagonales
				(TresEnRaya.gameBoard[0].equals("O") && 
				 TresEnRaya.gameBoard[4].equals("O") && 
				 TresEnRaya.gameBoard[8].equals("O")) ||
				(TresEnRaya.gameBoard[2].equals("O") && 
				 TresEnRaya.gameBoard[4].equals("O") && 
				 TresEnRaya.gameBoard[6].equals("O"))
		)
		{
			//si el cliente gana informamos por la ventana del chat
			setCellsEnabled(false);
			TresEnRaya.taChat.append(TresEnRaya.clientNick + " ha ganado!!!!" + "\n");
			GameOperations.scrollToBottom();
		}
		else
		{
			//comprobamos si hay empate
			if(TresEnRaya.movesNumber >= 9)
			{
				TresEnRaya.taChat.append("EMPATE!!!!");
				GameOperations.scrollToBottom();
			}
		}
	}
	
	
	
	/**
	 * activa o desactiva las celdas del tablero dependiendo 
	 * de si se ha inicado o no el juego para ello se debe 
	 * establecer conexión cliebnte <-> servidor
	 * @param b
	 */
	public static void setCellsEnabled(boolean active)
	{
		TresEnRaya.cell00.setEnabled(active);
		TresEnRaya.cell01.setEnabled(active);
		TresEnRaya.cell02.setEnabled(active);
		TresEnRaya.cell10.setEnabled(active);
		TresEnRaya.cell11.setEnabled(active);
		TresEnRaya.cell12.setEnabled(active);
		TresEnRaya.cell20.setEnabled(active);
		TresEnRaya.cell21.setEnabled(active);
		TresEnRaya.cell22.setEnabled(active);		
	}
	
	
	
	/**
	 * send a message from server to client or from client to server
	 * @param p
	 */
	public static void sendMessage(String message)
	{			
		try
		{
			TresEnRaya.objectOutputStream.writeObject(message);
			TresEnRaya.objectOutputStream.flush();
		}
		catch(SocketException e)
		{
			e.printStackTrace();
		}
		catch(Exception e) 
		{ 
			e.printStackTrace();
		}
	}
	
	

	/**
	 * cierra todas las conexiones y streams, termina el juego
	 */
	public static void closeConexions()
	{
		try { 
			TresEnRaya.objectOutputStream.flush();
			} catch (Exception e) { }
		try { 
			TresEnRaya.objectOutputStream.close();
			} catch (Exception e) { }
		try { 
			TresEnRaya.objectInputStream.close();
			} catch (Exception e) { }
		try { 
			TresEnRaya.serverSocket.close();
			} catch (Exception e) { }
		try { 
			TresEnRaya.clientSocket.close(); 
			} catch (Exception e) { }
	}
	


	/**
	 * Scroll al fondo del textArea al recibir un mensaje
	 */
	
	public static void scrollToBottom()
	{
		TresEnRaya.taChat.setCaretPosition(TresEnRaya.taChat.getText().length());
	}


}
