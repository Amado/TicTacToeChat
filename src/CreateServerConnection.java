

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;

	/**
	 * Clase que crea un socket de servidor en un nuevo thread para quedarse a la espera
	 * de la conexión de un cliente y manda mensajes al sistema
	 * @author emilio
	 *
	 */
	class CreateServerConnection implements Runnable
	{	
		public CreateServerConnection()
		{
			new Thread(this).start();
		}
		
		public void run()
		{
			try 
			{
				//iniciamos un nuevo socket de servidor
				TresEnRaya.serverSocket = new ServerSocket(TresEnRaya.port); 
				
				GameOperations.scrollToBottom();
				//aceptamos una nueva conexión de cliente
				TresEnRaya.clientSocket = TresEnRaya.serverSocket.accept();
				
				//iniciamos los streams del servidor
				TresEnRaya.objectOutputStream = new ObjectOutputStream(TresEnRaya.clientSocket.getOutputStream());
				TresEnRaya.objectOutputStream.flush();
				TresEnRaya.objectInputStream = new ObjectInputStream(TresEnRaya.clientSocket.getInputStream());
				
				//cunado el cliete se ha conectado informamos por la ventana de chat
				//de que todo está apunto
				GameOperations.sendMessage("Conectado al servidor!");
				TresEnRaya.taChat.append("Cliente conectado!\n");
				GameOperations.scrollToBottom();
				
				//turno de juego del servidor
				//escribimos una X
				TresEnRaya.xo = "X";
				TresEnRaya.whoseTurn = true;
				
				//informamos al sistema del nick usado
				TresEnRaya.serverNick = TresEnRaya.tfNick.getText();
				GameOperations.sendMessage(TresEnRaya.serverNick);
				
				//nick recibido del cliente
				TresEnRaya.chatMessage = (String) TresEnRaya.objectInputStream.readObject();
				TresEnRaya.clientNick = "" + TresEnRaya.chatMessage;
				
				//como todo el mudo tiene nick podemos iniciar el juego
				
				//habilitamos las celdas y el chat
				GameOperations.setCellsEnabled(true);
				TresEnRaya.tfChatMessage.setEditable(true);
				
				TresEnRaya.taChat.append("El servidor juega primero!\n");
				GameOperations.scrollToBottom();
				
				//deshabilitamos el panel superior, crear, unirse y nick
				TresEnRaya.btJoinGame.setEnabled(false);
				TresEnRaya.btCreateGame.setEnabled(false);
				TresEnRaya.tfNick.setEnabled(false);
				
				//thread para que el sistema reciba datos del cliente
				new ReceiveMessage("Mensaje del cliente"); 
			}
			catch (Exception e) 
			{ 
				e.printStackTrace();
			}
		}
	}