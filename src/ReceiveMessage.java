

	/**
	 * Clase que abre un nuevo thread para la recepción de un mensaje, estos mensajes, son tanto
	 * para el chat, como para que el sistema lleve el juego, tenemos que manadar entre apps
	 * la siguiente info:
	 * 
	 * 		mensajes entre server <-> client, el chat
	 * 		los turnos de juego
	 * 		cuando un jugador marca una celda, el otro debe ver esa acción
	 * 
	 * Para todo ello usamos un ObjectOutputStream y un ObjectInputStream, 
	 * toda la información comentada antes viaja por esos Streams entre server <-> user
	 * como es lógico, los Streams deben star asociados a sus respectivos sockets
	 * 
	 * @author emilio
	 *
	 */
	public class ReceiveMessage implements Runnable
	{	
		private boolean isThreadRunning;
		private String newMessage;
		
		public ReceiveMessage(String messageReceived)
		{
			isThreadRunning = true;
			newMessage = messageReceived;
			new Thread(this, newMessage).start();
		}
		
		public void run()
		{
			//dentro de un bucle infinito escuchamos 
			//constantemente los mensaje recibidos
			while(isThreadRunning)
			{
				try
				{
					TresEnRaya.chatMessage = "";
					
					//mensaje que se recibe
					TresEnRaya.chatMessage = (String) TresEnRaya.objectInputStream.readObject();
					
					//si el cliente recibe un mensaje del servidor
					if(newMessage.equals("Mensaje del servidor")) 
					{
						//damos turno al servidor o al cliente
						if(TresEnRaya.chatMessage.equalsIgnoreCase("true"))
						{
							TresEnRaya.whoseTurn = true;
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("false"))
						{
							TresEnRaya.whoseTurn = false;
						}
						//marcamos la celda y la deshabilitamos
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X1"))
						{
							TresEnRaya.cell00.setText("X");
							TresEnRaya.gameBoard[0] = "X";
							TresEnRaya.cell00.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X2"))
						{
							TresEnRaya.cell01.setText("X");
							TresEnRaya.gameBoard[1] = "X";
							TresEnRaya.cell01.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X3"))
						{
							TresEnRaya.cell02.setText("X");
							TresEnRaya.gameBoard[2] = "X";
							TresEnRaya.cell02.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X4"))
						{
							TresEnRaya.cell10.setText("X");
							TresEnRaya.gameBoard[3] = "X";
							TresEnRaya.cell10.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X5"))
						{
							TresEnRaya.cell11.setText("X");
							TresEnRaya.gameBoard[4] = "X";
							TresEnRaya.cell11.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X6"))
						{
							TresEnRaya.cell12.setText("X");
							TresEnRaya.gameBoard[5] = "X";
							TresEnRaya.cell12.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X7"))
						{
							TresEnRaya.cell20.setText("X");
							TresEnRaya.gameBoard[6] = "X";
							TresEnRaya.cell20.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X8"))
						{
							TresEnRaya.cell21.setText("X");
							TresEnRaya.gameBoard[7] = "X";
							TresEnRaya.cell21.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("X9"))
						{
							TresEnRaya.cell22.setText("X");
							TresEnRaya.gameBoard[8] = "X";
							TresEnRaya.cell22.setEnabled(false);
							GameOperations.checkCells();
						}
						//mensajes del chat
						else
						{
							TresEnRaya.taChat.append(TresEnRaya.serverNick + ":" + TresEnRaya.chatMessage + "\n");
							GameOperations.scrollToBottom();
						}
					}
					
					//si el servidor recibe un mensaje del cliente
					else if(newMessage.equals("Mensaje del cliente"))
					{
						//damos turno al cliente
						if(TresEnRaya.chatMessage.equalsIgnoreCase("true"))
						{
							TresEnRaya.whoseTurn = true;
						}
						//marcamos la celda y la deshabilitamos
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O1"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell00.setText("O");
							TresEnRaya.gameBoard[0] = "O";
							TresEnRaya.cell00.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O2"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell01.setText("O");
							TresEnRaya.gameBoard[1] = "O";
							TresEnRaya.cell01.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O3"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell02.setText("O");
							TresEnRaya.gameBoard[2] = "O";
							TresEnRaya.cell02.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O4"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell10.setText("O");
							TresEnRaya.gameBoard[3] = "O";
							TresEnRaya.cell10.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O5"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell11.setText("O");
							TresEnRaya.gameBoard[4] = "O";
							TresEnRaya.cell11.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O6"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell12.setText("O");
							TresEnRaya.gameBoard[5] = "O";
							TresEnRaya.cell12.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O7"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell20.setText("O");
							TresEnRaya.gameBoard[6] = "O";
							TresEnRaya.cell20.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O8"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell21.setText("O");
							TresEnRaya.gameBoard[7] = "O";
							TresEnRaya.cell21.setEnabled(false);
							GameOperations.checkCells();
						}
						else if(TresEnRaya.chatMessage.equalsIgnoreCase("O9"))
						{
							++TresEnRaya.movesNumber;
							TresEnRaya.cell22.setText("O");
							TresEnRaya.gameBoard[8] = "O";
							TresEnRaya.cell22.setEnabled(false);
							GameOperations.checkCells();
						}
						//mensajes del chat
						else
						{
							TresEnRaya.taChat.append(TresEnRaya.clientNick + ":" + TresEnRaya.chatMessage + "\n");
							GameOperations.scrollToBottom();
						}
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}